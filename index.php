<?php

//CONS
define('MYHWA_VERSION', '0.0.4');
define('MYHWA_APP', __FILE__);
define('MYHWA_APP_DIR', dirname(MYHWA_APP));
define('MYHWA_APP_ENV', 'LOCAL');

session_start();
if (isset($_SESSION['licznik']))
    $_SESSION['licznik'] ++;
else
    $_SESSION['licznik'] = 1;

if (file_exists(MYHWA_APP_DIR . '/config.php')) {
    require_once( MYHWA_APP_DIR . '/config.php' );
} else {
    echo('Plik konfiguracyjny nie istnieje!');
    die();
}