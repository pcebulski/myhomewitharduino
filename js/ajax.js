jQuery(document).ready(function ($) {
    $('.blind').click(function (event) {
        var blind = $(this);
        var blind_id = blind.attr('id').substr(3);
        var blind_status = blind.attr('data-status');

        $.ajax({
            url: '/app/rest-api',
            data: {
                'action': 'updateBlindOnClickAction',
                'blind_id': blind_id,
                'blind_value': blind_status
            },
            success: function (data) {
                if (data.blind_value === 'open') {
                    blind.removeClass('blind-close');
                    blind.addClass('blind-open');
                } else {
                    blind.removeClass('blind-open');
                    blind.addClass('blind-close');
                }
                blind.attr("data-status", data.blind_value);
                blind.find('.blind-value').text(data.blind_value);
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    $('.light').click(function (event) {
        var light = $(this);
        var light_id = light.attr('id').substr(3);
        var light_status = light.attr('data-status');
        var cubeSwitch = light.find('.cube-switch');
        var lightBulb2 = light.find('.light-bulb2');

        $.ajax({
            url: '/app/rest-api',
            data: {
                'action': 'updateLightOnClickAction',
                'light_id': light_id,
                'light_value': light_status
            },
            success: function (data) {
                if (data.light_value === 'on') {
                    light.removeClass('light-close');
                    light.addClass('light-open');
                } else {
                    light.removeClass('light-open');
                    light.addClass('light-close');
                }

                if (cubeSwitch.hasClass('active')) {
                    cubeSwitch.removeClass('active');
                    lightBulb2.removeClass('on');
                } else {
                    cubeSwitch.addClass('active');
                    lightBulb2.addClass('on');
                }

                light.attr("data-status", data.light_value);
                light.find('.light-value').text(data.light_value);
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    });

});