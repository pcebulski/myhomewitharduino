<?php

namespace src\includes\UserBundle\Entity;

/**
 * @author Paweł Cebulski
 */
class User {

    protected $id;
    protected $email;
    protected $password;

    public function __construct($id, $email, $password) {
        $this->setId($id);
        $this->setEmail($email);
        $this->setPassword($password);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set id
     * @param string $id
     * @return Light
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * Get email
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set email
     * @param string $email
     * @return User
     */
    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    /**
     * Get password
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set password
     * @param string $password
     * @return User
     */
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    function getUserByEmail($email) {
        global $mysql;
        $userResult = $mysql->query("SELECT * FROM pcebulski_myhome.users WHERE email = '$email'");

        if ($userResult->num_rows == 1) {
            $row = $userResult->fetch_assoc();
            $user = new User($row['id'], $row['email'], $row['password']);
            return $user;
        } else if ($userResult->num_rows == 0) {
            $_SESSION['alert'] = array(
                'type' => 'alert-primary',
                'content' => 'Nie ma takiego użytkownika.'
            );
            return false;
        } else {
            $_SESSION['alert'] = array(
                'type' => 'alert-primary',
                'content' => 'Więcej niż jedno konto z takim samym adresem mailowym?'
            );
        }
    }

}
