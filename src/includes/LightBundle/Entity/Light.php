<?php

namespace src\includes\LightBundle\Entity;

/**
 * @author Paweł Cebulski
 */
class Light {

    protected $id;
    protected $description;
    protected $value;
    protected $date_create;
    protected $date_last_update;
    protected $pin_out;

    public function __construct($id, $description, $value, $date_create, $date_last_update, $pin_out) {
        $this->setId($id);
        $this->setDescription($description);
        $this->setValue($value);
        $this->setDateCreate($date_create);
        $this->setDateLastUpdate($date_last_update);
        $this->setPinOut($pin_out);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set id
     * @param string $id
     * @return Light
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return Light
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * Get value
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set value
     * @param string $value
     * @return Light
     */
    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    /**
     * Get dateCreate
     * @return string
     */
    public function getDateCreate() {
        return $this->date_create;
    }

    /**
     * Set dateCreate
     * @param string $date_create
     * @return Light
     */
    public function setDateCreate($date_create) {
        $this->date_create = $date_create;
        return $this;
    }

    /**
     * Get dateLastUpdate
     * @return string
     */
    public function getDateLastUpdate() {
        return $this->date_last_update;
    }

    /**
     * Set dateLastUpdate
     * @param string $date_last_update
     * @return Light
     */
    public function setDateLastUpdate($date_last_update) {
        $this->date_last_update = $date_last_update;
        return $this;
    }

    /**
     * Get pinOut
     * @return integer
     */
    public function getPinOut() {
        return $this->pin_out;
    }

    /**
     * Set pinOut
     * @param string $pinOut
     * @return Blind
     */
    public function setPinOut($pinOut) {
        $this->pin_out = $pinOut;
        return $this;
    }

    function getLightCollection() {
        global $mysql;
        $lightCollection = $mysql->query("SELECT * FROM pcebulski_myhome.myhwa_light_options");

        if ($lightCollection->num_rows > 0) {
            $lightArrayCollection = [];
            while ($row = $lightCollection->fetch_assoc()) {
                $light = new Light($row['id'], $row['description'], $row['value'], $row['date_create'], $row['date_last_update'], $row['pin_out']);
                array_push($lightArrayCollection, $light);
            }
            return $lightArrayCollection;
        } else {
            return false;
        }
    }

    public function getLightById($id) {
        global $mysql;
        $sqlResult = $mysql->query("SELECT * FROM pcebulski_myhome.myhwa_light_options WHERE id = $id LIMIT 1");
        if ($sqlResult->num_rows > 0) {
            $row = $sqlResult->fetch_assoc();
            $light = new Light($row['id'], $row['description'], $row['value'], $row['date_create'], $row['date_last_update'], $row['pin_out']);
            return $light;
        } else {
            return NULL;
        }
    }

    public function save() {
        global $mysql;
        $sql = "UPDATE pcebulski_myhome.myhwa_light_options"
                . " SET"
                . " `description`='" . $this->getDescription() . "',"
                . " `value`='" . $this->getValue() . "',"
                . " `date_last_update`='" . $this->getDateLastUpdate() . "'"
                . "  WHERE id = " . $this->getId() . "";
        return $mysql->query($sql);
    }

}
