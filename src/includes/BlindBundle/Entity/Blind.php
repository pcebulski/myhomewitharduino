<?php

namespace src\includes\BlindBundle\Entity;

/**
 * @author Paweł Cebulski
 */
class Blind {

    protected $id;
    protected $description;
    protected $value;
    protected $date_create;
    protected $date_last_update;
    protected $pin_out;

    public function __construct($id, $description, $value, $date_create, $date_last_update, $pin_out) {
        $this->setId($id);
        $this->setDescription($description);
        $this->setValue($value);
        $this->setDateCreate($date_create);
        $this->setDateLastUpdate($date_last_update);
        $this->setPinOut($pin_out);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set id
     * @param string $id
     * @return Blind
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return Blind
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * Get value
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set value
     * @param string $value
     * @return Blind
     */
    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    /**
     * Get dateCreate
     * @return string
     */
    public function getDateCreate() {
        return $this->date_create;
    }

    /**
     * Set dateCreate
     * @param string $date_create
     * @return Blind
     */
    public function setDateCreate($date_create) {
        $this->date_create = $date_create;
        return $this;
    }

    /**
     * Get dateLastUpdate
     * @return string
     */
    public function getDateLastUpdate() {
        return $this->date_last_update;
    }

    /**
     * Set dateLastUpdate
     * @param string $date_last_update
     * @return Blind
     */
    public function setDateLastUpdate($date_last_update) {
        $this->date_last_update = $date_last_update;
        return $this;
    }

    /**
     * Get pinOut
     * @return integer
     */
    public function getPinOut() {
        return $this->pin_out;
    }

    /**
     * Set pinOut
     * @param string $pinOut
     * @return Blind
     */
    public function setPinOut($pinOut) {
        $this->pin_out = $pinOut;
        return $this;
    }

    function getBlindCollection() {
        global $mysql;
        $blindCollection = $mysql->query("SELECT * FROM pcebulski_myhome.myhwa_blind_options");

        if ($blindCollection->num_rows > 0) {
            $blindArrayCollection = [];
            while ($row = $blindCollection->fetch_assoc()) {
                $blind = new Blind($row['id'], $row['description'], $row['value'], $row['date_create'], $row['date_last_update'], $row['pin_out']);
                array_push($blindArrayCollection, $blind);
            }
            return $blindArrayCollection;
        } else {
            return false;
        }
    }

    public function getBlindById($id) {
        global $mysql;
        $sqlResult = $mysql->query("SELECT * FROM pcebulski_myhome.myhwa_blind_options WHERE id = $id LIMIT 1");
        if ($sqlResult->num_rows > 0) {
            $row = $sqlResult->fetch_assoc();
            $blind = new Blind($row['id'], $row['description'], $row['value'], $row['date_create'], $row['date_last_update'], $row['pin_out']);
            return $blind;
        } else {
            return NULL;
        }
    }

    public function save() {
        global $mysql;
        $sql = "UPDATE pcebulski_myhome.myhwa_blind_options"
                . " SET"
                . " `description`='" . $this->getDescription() . "',"
                . " `value`='" . $this->getValue() . "',"
                . " `date_last_update`='" . $this->getDateLastUpdate() . "'"
                . "  WHERE id = " . $this->getId() . "";
        return $mysql->query($sql);
    }

}
