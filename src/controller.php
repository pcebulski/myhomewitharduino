<?php

$urlArray = explode('/', $_SERVER['REQUEST_URI']);
array_shift($urlArray);
if (isset($urlArray[0])) {
    $urlModul = $urlArray[0];
}
if (isset($urlArray[1])) {
    $urlClass = $urlArray[1];
}
if (isset($urlArray[2])) {
    $urlValue = $urlArray[2];
}

if ($urlModul == "") {
    if (!isUserLogin()) {
        $urlModul = 'login';
        $urlClass = 'login';
    } else {
        $urlModul = 'index';
        $urlClass = 'index';
    }
} elseif ($urlModul == 'app') {
    $parseUrl = parse_url($urlClass);
    $urlClass = $parseUrl['path'];
}

//if (isset($urlModul)) {
//    var_dump($urlModul);
//}
//if (isset($urlClass)) {
//    var_dump($urlClass);
//}
//if (isset($urlValue)) {
//    var_dump($urlValue);
//}

if (!file_exists(MYHWA_APP_DIR . '/src/modules/' . $urlModul . '/' . $urlClass . '.php')) {
    $urlModul = 'index';
    $urlClass = '404';
}

$loadTemplate = TRUE;
if ($urlClass == 'rest-api') {
    $loadTemplate = FALSE;
}

if ($loadTemplate) {
    include_once(MYHWA_APP_DIR . '/src/header.php');
}
include_once(MYHWA_APP_DIR . '/src/modules/' . $urlModul . '/' . $urlClass . '.php');
if ($loadTemplate) {
    include_once(MYHWA_APP_DIR . '/src/footer.php');
}