<!DOCTYPE html>
<html class="no-js" lang="pl">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Paweł Cebulski">

        <title>My Home With Arduino</title>

        <link rel="icon" type="image/ico" href="/img/favicon.ico" />
        <!-- Bootstrap core CSS -->
        <link type="text/css" rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
        <!-- Custom fonts for this template -->
        <link type="text/css" rel="stylesheet" href="/vendor/fontawesome-free/css/all.min.css">
        <link type="text/css" rel="stylesheet" href="/vendor/simple-line-icons/css/simple-line-icons.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli">
        <!-- Plugin CSS -->
        <!--<link rel="stylesheet" href="device-mockups/device-mockups.min.css" type="text/css">-->

        <!-- Custom styles for this template -->
        <link type="text/css" rel="stylesheet" href="/css/new-age.css">
        <link type="text/css" rel="stylesheet" href="/css/style.css">               
    </head>
    <body>
        <?php include_once(MYHWA_APP_DIR . '/src/includes/navigation.php'); ?>