<?php

use src\includes\BlindBundle\Entity\Blind;
use src\includes\LightBundle\Entity\Light;
use src\includes\UserBundle\Entity\User;

require_once(MYHWA_APP_DIR . '/src/includes/BlindBundle/Entity/Blind.php');
require_once(MYHWA_APP_DIR . '/src/includes/LightBundle/Entity/Light.php');
require_once(MYHWA_APP_DIR . '/src/includes/UserBundle/Entity/User.php');

function isUserLogin() {
    if (isset($_SESSION['login_user'])) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function printAlerts() {
    if (isset($_SESSION['alert'])) {
        echo '<div class="alert ' . $_SESSION['alert']['type'] . '" role="alert">'
        . $_SESSION['alert']['content']
        . '</div>';
        unset($_SESSION['alert']);
    }
}

function printBlindsBoxes() {
    $blindCollection = Blind::getBlindCollection();

    $numOfCols = 4;
    $rowCount = 0;
    $html = '<div class="row">';
    foreach ($blindCollection as $blind) {
        $bootstrapColWidth = 12 / $numOfCols;
        $html .= '<div class="blind blind-' . $blind->getValue() . ' col-md-' . $bootstrapColWidth . '" id="id_' . $blind->getId() . '" data-status="' . $blind->getValue() . '">';
        $html .= '<h4>' . $blind->getDescription() . '</h4>';
        $html .= '<div class="option-image"></div>';
        $html .= '<span class="blind-value">' . $blind->getValue() . '</span>';
        $html .= '</div>';
        $rowCount++;
        if ($rowCount % $numOfCols == 0) {
            $html .= '</div><div class="row">';
        }
    }
    $html .= '</div>';
    return $html;
}

function printLightBoxes() {
    $lightCollection = Light::getLightCollection();

    $numOfCols = 4;
    $rowCount = 0;
    $html = '<div class="row">';
    foreach ($lightCollection as $light) {
        $swithClass = '';
        if ($light->getValue() == 'on') {
            $swithClass = 'active';
        }
        $bootstrapColWidth = 12 / $numOfCols;
        $html .= '<div class="light light-' . $light->getValue() . ' col-md-' . $bootstrapColWidth . '" id="id_' . $light->getId() . '" data-status="' . $light->getValue() . '">';
        $html .= '<h4>' . $light->getDescription() . '</h4>';
        $html .= '<div class="light-bulb ui-draggable ' . $light->getValue() . '" ><div class="light-bulb2 ' . $light->getValue() . '"></div></div>';
        $html .= '<div href="" class="cube-switch ' . $swithClass . '">';
        $html .= '<span class="switch">';
        $html .= '<span class="switch-state off">Off</span>';
        $html .= '<span class="switch-state on">On</span>';
        $html .= '</span>';
        $html .= '</div>';
//        $html .= '<span class="light-value">' . $light->getValue() . '</span>';
        $html .= '</div>';
        $rowCount++;
        if ($rowCount % $numOfCols == 0) {
            $html .= '</div><div class="row">';
        }
    }
    $html .= '</div>';
    return $html;
}

function getArduino() {
    global $mysql;
    $arduino = $mysql->query("SELECT * FROM pcebulski_myhome.arduino_options");
    if ($arduino->num_rows > 0) {
        $obj_arduino = [];
        while ($row = $arduino->fetch_assoc()) {
            $tmp = (object) [
                        'id' => $row['id'],
                        'type' => $row['type'],
                        'title' => $row['title'],
                        'value' => $row['value']
            ];
            array_push($obj_arduino, $tmp);
        }
        return $obj_arduino;
    } else {
        return false;
    }
}

function getAllArduinoOptions() {
    global $mysql;
    $options = $mysql->query("SELECT * FROM pcebulski_myhome.arduino_options");
    if ($options->num_rows > 0) {
        $result_options = [];
        while ($option = $options->fetch_assoc()) {
            $tmp = (object) [
                        'id' => $option['id'],
                        'type' => $option['type'],
                        'title' => $option['title'],
                        'value' => $option['value'],
//                        'amount' => $options['amount'],
//                        'description' => $options['description'],
//                        'status' => $options['status'],
            ];
            array_push($result_options, $tmp);
        }
        return $result_options;
    } else {
        return false;
    }
}

function printOptionsTable($options) {
    ?>
    <table>
        <caption>Lista ustawień</caption>
        <thead>
            <tr>
                <td>ID</td>
                <td>Type</td>
                <td>Title</td>
                <td>Value</td>
                <td>Opcje</td>
            </tr>
        </thead>
        <?php
        foreach ($options as $option) {
            ?>
            <tr>
                <td><?php echo $option->id ?></td>
                <td><?php echo $option->type ?></td>
                <td><?php echo $option->title ?></td>
                <td><?php echo $option->value ?></td>
                <td></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
}

function redirect($url) {
    echo "<script type='text/javascript'>location.href = '$url';</script>";
}

function checkPass($sendEmail, $sendPass) {
    $user = User::getUserByEmail($sendEmail);
    $return = false;
    if ($user) {
        if (strcmp($sendPass, decrypt($user->getPassword())) == 0) {
            $return = true;
        }
    }
    return $return;
}

function encrypt($plaintext) {
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, AUTH_KEY, $options = OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, AUTH_KEY, $as_binary = true);
    $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);
    return $ciphertext;
}

function decrypt($ciphertext) {
    $c = base64_decode($ciphertext);
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len = 32);
    $ciphertext_raw = substr($c, $ivlen + $sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, AUTH_KEY, $options = OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, AUTH_KEY, $as_binary = true);
    if (hash_equals($hmac, $calcmac)) {//PHP 5.6+ timing attack safe comparison
        return $original_plaintext;
    }
}
