<?php
if (isUserLogin()) {
    redirect("/");
}
if (isset($_POST['submit'])) {
    if (checkPass($_POST['username'], $_POST['password'])) {
        $_SESSION['login_user'] = $_POST['username'];
        $_SESSION['komunikat'] = 'Zostałeś poprawnie zalogowany.';
        redirect("/");
    }
}
?>
<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card">
            <div class="card-header">
                <h3>Zaloguj się</h3>
            </div>
            <div class="card-body">
                <form method="post" action="/login/login">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-user"></i>
                            </span>
                        </div>
                        <input id="username" name="username" type="text" placeholder="username" class="form-control">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-key"></i>
                            </span>
                        </div>
                        <input id="password" name="password" type="password" placeholder="password" class="form-control" >
                    </div>
                    <div class="row align-items-center remember">
                        <input id="remember-me" name="remember-me" type="checkbox">Zapamiętaj mnie
                    </div>
                    <div class="form-group">                        
                        <button id="submit" value="login" name="submit" class="btn float-right login_btn">Zaloguj</button>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-center links">
                    Kontakt z administratorem:
                </div>    
                <div class="d-flex justify-content-center">
                    <a href="mailto:p.cebulski.poczta@gmail.com">p.cebulski.poczta@gmail.com</a>
                </div>
            </div>
            <?php
            printAlerts();
            ?>
        </div>
    </div>
</div>