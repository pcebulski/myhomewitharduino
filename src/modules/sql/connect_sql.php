<?php
global $mysql;
$mysql = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);

// Check connection
if ($mysql->connect_error) {
    die("Connection failed: " . $mysql->connect_error);
}

