-- --------------------------------------------------------
-- Host:                         mysql-579094.vipserv.org
-- Wersja serwera:               10.3.12-MariaDB - MariaDB Server
-- Serwer OS:                    Linux
-- HeidiSQL Wersja:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Zrzut struktury bazy danych pcebulski_myhome
CREATE DATABASE IF NOT EXISTS `pcebulski_myhome` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci */;
USE `pcebulski_myhome`;

-- Zrzut struktury tabela pcebulski_myhome.myhwa_blind_options
CREATE TABLE IF NOT EXISTS `myhwa_blind_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` TEXT NULL DEFAULT NULL,
  `value` TEXT(255) NULL DEFAULT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `date_last_update` DATETIME NULL DEFAULT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista ustawień rolet';

-- Zrzucanie danych dla tabeli pcebulski_myhome.myhwa_blind_options: ~0 rows (około)
/*!40000 ALTER TABLE `myhwa_blind_options` DISABLE KEYS */;
INSERT INTO `myhwa_blind_options` (`id`, `description`, `value`, `date_create`, `date_last_update`) VALUES
	(1, 'Kuchnia', 'open', '2019-01-18', '2019-01-18'),
	(2, 'Jadalnia', 'open', '2019-01-18', '2019-01-18'),
	(3, 'Salon', 'open', '2019-01-18', '2019-01-18'),
	(4, 'Sypialnia', 'open', '2019-01-18', '2019-01-18');

-- Zrzut struktury tabela pcebulski_myhome.myhwa_light_options
CREATE TABLE IF NOT EXISTS `myhwa_light_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` TEXT NULL DEFAULT NULL,
  `value` TEXT(255) NULL DEFAULT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `date_last_update` DATETIME NULL DEFAULT NULL,  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista ustawień świateł';

-- Zrzucanie danych dla tabeli pcebulski_myhome.myhwa_light_options: ~0 rows (około)
/*!40000 ALTER TABLE `myhwa_light_options` DISABLE KEYS */;
INSERT INTO `myhwa_light_options` (`id`, `description`, `value`, `date_create`, `date_last_update`) VALUES
	(1, 'Kuchnia', 'off', '2019-01-18', '2019-01-18'),
	(2, 'Jadalnia', 'off', '2019-01-18', '2019-01-18'),
	(3, 'Salon', 'off', '2019-01-18', '2019-01-18'),
	(4, 'Sypialnia', 'off', '2019-01-18', '2019-01-18');

/*!40000 ALTER TABLE `arduino_options` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
ALTER TABLE `myhwa_blind_options` ADD COLUMN `pin_out` INT NOT NULL DEFAULT '0' AFTER `date_last_update`;
ALTER TABLE `myhwa_blind_options` ADD UNIQUE INDEX `pin_out` (`pinOut`);
ALTER TABLE `myhwa_light_options` ADD COLUMN `pin_out` INT NOT NULL DEFAULT '0' AFTER `date_last_update`;
ALTER TABLE `myhwa_light_options` ADD UNIQUE INDEX `pin_out` (`pinOut`);

CREATE TABLE IF NOT EXISTS `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `email` VARCHAR(40) UNIQUE,
    `password` VARCHAR(255) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lista ustawień rolet';

INSERT INTO `users` (`email`, `password`) VALUES
    ('p.cebulski.poczta@gmail.com', '123');