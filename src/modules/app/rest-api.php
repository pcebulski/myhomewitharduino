<?php

use src\includes\BlindBundle\Entity\Blind;
use src\includes\LightBundle\Entity\Light;

if ($_REQUEST['action']) {
    try {
        $func = $_REQUEST['action'];
        $func($_REQUEST);
    } catch (Throwable $t) {
        echo 'Caught Throwable: ', $t->getMessage(), "\n";
    } catch (Exception $e) {
        echo 'Caught Exception: ', $e->getMessage(), "\n";
    }
}

/**
 * Żądanie pobierające stan wszystkich urządzeń
 * @param type $request
 */
function getAllDevices($request = NULL) {
    if (isset($request)) {
        $return = [];
        $blindCollection = Blind::getBlindCollection();
        $lightCollection = Light::getLightCollection();

        foreach ($lightCollection as $light) {
            $temp = array(
                'p' => $light->getPinOut(),
                'v' => $light->getValue(),
            );
            $return[] = $temp;
        }

        foreach ($blindCollection as $blind) {
            $temp = array(
                'p' => $blind->getPinOut(),
                'v' => $blind->getValue(),
            );
            $return[] = $temp;
        }
    } else {
        $return = "FAIL";
    }
    echo jsonSerialResponse($return);
}

/**
 * UNUSED
 * Żądanie pobierające aktualną wartośc rolety.
 * @param type $request
 */
function getBlind($request = NULL) {
    if (isset($request)) {
        $blind_id = $request['id'];
        $blind = Blind::getBlindById($blind_id);
        $return = array(
            'id' => $blind->getId(),
            'value' => $blind->getValue(),
        );
    } else {
        $return = "FAIL";
    }
    echo jsonSerialResponse($return);
}

/**
 * Żądanie pobierające stan wszystkich urządzeń
 * @param type $request
 */
function getDevice($request = NULL) {
    if (isset($request)) {
        $devicePinOut = $request['id'];
        $return = [];
        $blindCollection = Blind::getBlindCollection();
        $lightCollection = Light::getLightCollection();
        foreach ($blindCollection as $blind) {
            if ($blind->getPinOut() == $devicePinOut) {
                $return = $blind->getValue();
                break;
            }
        }
        foreach ($lightCollection as $light) {
            if ($light->getPinOut() == $devicePinOut) {
                $return = $light->getValue();
                break;
            }
        }
    } else {
        $return = "FAIL";
    }
    echo jsonSerialResponse($return);
}

/** UNUSED
 * Żądanie pobierające stan wszystkich urządzeń
 * @param type $request
 */
function getDevicesAmount($request = NULL) {
    if (isset($request)) {
        $return = [];
        $blindCollection = Blind::getBlindCollection();
        $lightCollection = Light::getLightCollection();
        $devicesAmount = 0;
        foreach ($blindCollection as $blind) {
            $devicesAmount++;
        }
        foreach ($lightCollection as $light) {
            $devicesAmount++;
        }
    } else {
        $return = "FAIL";
    }
    echo jsonSerialResponse($devicesAmount);
}

/**
 * 
 * Fankcja aktualizuje stan rolety
 * @category Arduino
 * @param type $request
 */
function updateBlind($request = NULL) {
    if (isset($request)) {
        $blind_id = $request['id'];
        $blind_value = $request['value'];
        $blind = Blind::getBlindById($blind_id);
        if ($blind_value == 'open') {
            $blind_new_value = 'close';
        } else {
            $blind_new_value = 'open';
        }
        $blind->setValue($blind_new_value);
        $blind->setDateLastUpdate(date("Y-m-d H:i:s"));
        $blind->save();
        $return = array(
            'id' => $blind_id,
            'blind_value' => $blind_new_value
        );
    } else {
        $return = "FAIL";
    }
    echo json_response($return);
}

/**
 * Aktualizuje stan rolety po kliknięciu
 * @category Ajax
 * @param type $request
 */
function updateBlindOnClickAction($request = NULL) {
    if (isset($request)) {
        $blind_id = $request['blind_id'];
        $blind_value = $request['blind_value'];
        $blind = Blind::getBlindById($blind_id);
        if ($blind_value == 'open') {
            $blind_new_value = 'close';
        } else {
            $blind_new_value = 'open';
        }
        $blind->setValue($blind_new_value);
        $blind->setDateLastUpdate(date("Y-m-d H:i:s"));
        $blind->save();
        $return = array(
            'id' => $blind_id,
            'blind_value' => $blind_new_value
        );
    } else {
        $return = "FAIL";
    }
    echo json_response($return);
}

/**
 * Aktualizuje oświetlenie po kliknięciu.
 * @category Ajax
 * @param type $request
 */
function updateLightOnClickAction($request = NULL) {
    if (isset($request)) {
        $light_id = $request['light_id'];
        $light_value = $request['light_value'];
        $light = Light::getLightById($light_id);
        if ($light_value == 'on') {
            $light_new_value = 'off';
        } else {
            $light_new_value = 'on';
        }
        $light->setValue($light_new_value);
        $light->setDateLastUpdate(date("Y-m-d H:i:s"));
        $light->save();
        $return = array(
            'id' => $light_id,
            'light_value' => $light_new_value
        );
    } else {
        $return = "FAIL";
    }
    echo json_response($return);
}

/**
 * Funkcja ułatwiające preparowanie nagłówków odpowiedzi.
 * @param type $return
 * @param type $message
 * @param type $code
 * @return type
 */
function json_response($return = null, $message = null, $code = 200) {
    header_remove();
    http_response_code($code);
    header("Cache-Control: no-cache,no-transform,public");
    header('Content-Type: application/json');
    $status = array(
        200 => '200 OK',
        400 => '400 Bad Request',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error'
    );
    header('Status: ' . $status[$code]);
    return json_encode(array_merge(array('status' => $code < 300, 'message' => $message), $return));
}

/**
 * Funkcja odpowiadająca dla ESP.
 * @param type $return
 * @param type $message
 * @param type $code
 * @return type
 */
function jsonSerialResponse($return = null, $code = 200) {
    header_remove();
    http_response_code($code);
    header("Cache-Control: no-cache,no-transform,public");
    header('Content-Type: application/json');
    $status = array(
        200 => '200 OK',
        400 => '400 Bad Request',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error'
    );
    header('Status: ' . $status[$code]);
    echo "<" . json_encode($return) . ">";
}

/**
 * Żądanie pobierające aktualną wartośc rolety.
 * @param type $request
 */
function getTestJson($request = NULL) {
    echo "{\"status\":true,\"message\":\"ArduinoRequest\",\"id\":\"3\",\"value\":\"open\"}";
//    echo json_response("{\"status\":true,\"message\":\"ArduinoRequest\",\"id\":\"3\",\"value\":\"open\"}");
}
