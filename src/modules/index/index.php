<header class="masthead">
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-lg-12 my-auto">
                <h2 class="section-heading">Rolety</h2>
                <?php echo printBlindsBoxes(); ?>
            </div>   
            <div class="col-lg-12 my-auto">
                <h2 class="section-heading">Światła</h2>
                <?php echo printLightBoxes(); ?>
            </div>  
        </div>
    </div>
</header>