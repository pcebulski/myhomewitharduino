<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h1>
                    Oops!</h1>
                <h2>
                    404 Strona nie istnieje</h2>
                <div class="error-details">
                    Przepraszamy, wystąpił błąd, nie znaleziono strony z żądaniem!
                </div>
                <div class="error-actions">
                    <a href="/" class="btn btn-primary">
                        Powrót do strony głównej
                    </a>
                    <a href="mailto:p.cebulski.poczta@gmail.com" class="btn btn-secondary">
                        Kontakt z administratorem
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>