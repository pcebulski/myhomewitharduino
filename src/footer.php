<?php printAlerts(); ?>

<?php if (isUserLogin()) { ?>
    <!-- Bootstrap core JavaScript -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/js/new-age.min.js"></script>
    
    <script src="/js/ajax.js"></script>
    <script src="/js/home.js"></script>
<?php } ?>

</body>
</html>