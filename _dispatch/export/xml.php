<?php
header('Content-type: text/xml');

$download_xml = false;

$xml = new SimpleXMLElement('<PRODUCTS/>');

$products = get_all_products();

foreach ($products as $product) {
    $xml_product = $xml->addChild('PRODUCT');
    $xml_product->addChild('ID', $product->id);
    $xml_product->addChild('EAN', $product->ean);
    $xml_product->addChild('NAME', $product->name);
    $xml_product->addChild('PRICE', $product->price);
}

if ($download_xml) {
    header('Content-Disposition: attachment; filename="products.xml"');
}
echo $xml->asXML();
exit();
