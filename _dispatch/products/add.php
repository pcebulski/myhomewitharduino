<?php

$product = $_POST;
global $error;
foreach ($product as $field_name => $field_value) {
    if (!$field_value || $field_value = '') {
        $error = 'Formularz wypełniony niepoprawnie - pole ' . $field_name . ' nie zostało uzupełnione!';
        echo $error;
        print_back_to_home_btn();
        exit();
    }
}
add_new_product($product);
print_back_to_home_btn();
//header("Location: /");